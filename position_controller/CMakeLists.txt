cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME position_controller)
project(${PROJECT_NAME})


### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake
add_definitions(-std=c++11)
#add_definitions(-std=c++0x)
#add_definitions(-std=c++03)
add_definitions(-g)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries

# Compiler-specific C++11 activation.
if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "GNU")
    execute_process(
        COMMAND ${CMAKE_CXX_COMPILER} -dumpversion OUTPUT_VARIABLE GCC_VERSION)
    if (NOT (GCC_VERSION VERSION_GREATER 4.7 OR GCC_VERSION VERSION_EQUAL 4.7))
        message(FATAL_ERROR "${PROJECT_NAME} requires g++ 4.7 or greater.")
    endif ()
elseif ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
else ()
    message(FATAL_ERROR "Your C++ compiler does not support C++11.")
endif ()

set(POSITION_CONTROLLER_SOURCE_DIR src/source) 
set(POSITION_CONTROLLER_INCLUDE_DIR src/include)

set(POSITION_CONTROLLER_HEADER_FILES

	#General
        ${POSITION_CONTROLLER_INCLUDE_DIR}/position_controller.h
	)

set(POSITION_CONTROLLER_SOURCE_FILES
  
	#General
        ${POSITION_CONTROLLER_SOURCE_DIR}/position_controller.cpp
 )

find_package(catkin REQUIRED COMPONENTS roscpp pid_control)

catkin_package(
	INCLUDE_DIRS ${POSITION_CONTROLLER_INCLUDE_DIR}
                LIBRARIES position_controller
  CATKIN_DEPENDS roscpp pid_control
		DEPENDS
)

include_directories(${POSITION_CONTROLLER_INCLUDE_DIR})
include_directories(${catkin_INCLUDE_DIRS})

add_library(position_controller ${POSITION_CONTROLLER_SOURCE_FILES} ${POSITION_CONTROLLER_HEADER_FILES})
add_dependencies(position_controller ${catkin_EXPORTED_TARGETS})
target_link_libraries(position_controller ${catkin_LIBRARIES})

