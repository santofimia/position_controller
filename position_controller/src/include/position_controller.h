#ifndef position_controller
#define position_controller

#include "pid_control.h"

/*!*************************************************************************************
 *  \class     position_controller
 *
 *  \brief     Position Controller PID
 *
 *  \details   This class is in charge of control position XY using PID class.
 *             Sends as Outputs speed references.
 *
 *
 *  \authors   Pablo Santofimia Ruiz
 *
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ***************************************************************************************/

#include <ros/ros.h>

#include "xmlfilereader.h"


class PositionController
{
public:

  void setUp();

  void start();

  void stop();

  void setFeedback(float posx, float posy );
  void setReference(float ref_posx, float ref_posy);
  void getOutput(float *dx, float *dy);

  //! Read Config
  bool readConfigs(std::string configFile);

  //! Constructor. \details Same arguments as the ros::init function.
  PositionController();

  //! Destructor.
  ~PositionController();

private:

  PID PID_X2Dx;
  PID PID_Y2Dy;

  //! Configuration file variable
  int idDrone;               // Id Drone integer (number of the drone)
  std::string posconfigFile;    // Config File String name
  std::string stackPath;     // Config File String aerostack path name


  float pid_x2dx_kp;
  float pid_x2dx_ki;
  float pid_x2dx_kd;
  bool pid_x2dx_enablesat;
  float pid_x2dx_satmax;
  float pid_x2dx_satmin;
  bool pid_x2dx_enableantiwp;
  float pid_x2dx_kw;

  float pid_y2dy_kp;
  float pid_y2dy_ki;
  float pid_y2dy_kd;
  bool pid_y2dy_enablesat;
  float pid_y2dy_satmax;
  float pid_y2dy_satmin;
  bool pid_y2dy_enableantiwp;
  float pid_y2dy_kw;

};
#endif 
