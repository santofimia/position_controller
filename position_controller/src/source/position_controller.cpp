#include "position_controller.h"

//Constructor
PositionController::PositionController()
{
    std::cout << "Constructor: PositionController" << std::endl;

}

//Destructor
PositionController::~PositionController() {}



bool PositionController::readConfigs(std::string configFile)
{

    try
    {


    XMLFileReader my_xml_reader(configFile);


    /*********************************  Position X Controller ( from X to Dx ) ************************************************/

    // Gain
    pid_x2dx_kp = my_xml_reader.readDoubleValue("Position_Controller:PID_X2Dx:Gain:Kp");
    pid_x2dx_ki = my_xml_reader.readDoubleValue("Position_Controller:PID_X2Dx:Gain:Ki");
    pid_x2dx_kd = my_xml_reader.readDoubleValue("Position_Controller:PID_X2Dx:Gain:Kd");
    pid_x2dx_enablesat = my_xml_reader.readDoubleValue("Position_Controller:PID_X2Dx:Saturation:enable_saturation");
    pid_x2dx_satmax = my_xml_reader.readDoubleValue("Position_Controller:PID_X2Dx:Saturation:SatMax");
    pid_x2dx_satmin = my_xml_reader.readDoubleValue("Position_Controller:PID_X2Dx:Saturation:SatMin");
    pid_x2dx_enableantiwp = my_xml_reader.readDoubleValue("Position_Controller:PID_X2Dx:Anti_wind_up:enable_anti_wind_up");
    pid_x2dx_kw = my_xml_reader.readDoubleValue("Position_Controller:PID_X2Dx:Anti_wind_up:Kw");


    /*********************************  Position Y Controller ( from Y to Dy ) **************************************************/

    // Gain
    pid_y2dy_kp = my_xml_reader.readDoubleValue("Position_Controller:PID_Y2Dy:Gain:Kp");
    pid_y2dy_ki = my_xml_reader.readDoubleValue("Position_Controller:PID_Y2Dy:Gain:Ki");
    pid_y2dy_kd = my_xml_reader.readDoubleValue("Position_Controller:PID_Y2Dy:Gain:Kd");
    pid_y2dy_enablesat = my_xml_reader.readDoubleValue("Position_Controller:PID_Y2Dy:Saturation:enable_saturation");
    pid_y2dy_satmax = my_xml_reader.readDoubleValue("Position_Controller:PID_Y2Dy:Saturation:SatMax");
    pid_y2dy_satmin = my_xml_reader.readDoubleValue("Position_Controller:PID_Y2Dy:Saturation:SatMin");
    pid_y2dy_enableantiwp = my_xml_reader.readDoubleValue("Position_Controller:PID_Y2Dy:Anti_wind_up:enable_anti_wind_up");
    pid_y2dy_kw = my_xml_reader.readDoubleValue("Position_Controller:PID_Y2Dy:Anti_wind_up:Kw");


    }


    catch ( cvg_XMLFileReader_exception &e)
    {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }


    return true;
}

void PositionController::setUp()
{

    ros::param::get("~stackPath", stackPath);
    if ( stackPath.length() == 0)
    {
        stackPath = "$(env AEROSTACK_STACK)";
    }
    ros::param::get("~droneId", idDrone);
    ros::param::get("~pos_config_file", posconfigFile);
    if ( posconfigFile.length() == 0)
    {
        posconfigFile="position_controller.xml";
    }

    bool readConfigsBool = readConfigs(stackPath+"/configs/drone"+cvg_int_to_string(idDrone)+"/"+posconfigFile);

    if(!readConfigsBool)
    {
        std::cout << "Error init"<< std::endl;
        return;
    }

    std::cout << "Constructor: PositionController...Exit" << std::endl;


}

void PositionController::start()
{

    // Reset PID
    PID_X2Dx.reset();
    PID_Y2Dy.reset();

}

void PositionController::stop()
{

}


void PositionController::setFeedback(float posx, float posy ){
    PID_X2Dx.setFeedback(posx);
    PID_Y2Dy.setFeedback(posy);
}
void PositionController::setReference(float ref_posx, float ref_posy){
    PID_X2Dx.setReference(ref_posx);
    PID_Y2Dy.setReference(ref_posy);

}

void PositionController::getOutput(float *dx, float *dy){

    /************************ Position X Controller ( from X to Dx )   *****************************/

    PID_X2Dx.setGains(pid_x2dx_kp,pid_x2dx_ki,pid_x2dx_kd);
    PID_X2Dx.enableMaxOutput(pid_x2dx_enablesat,pid_x2dx_satmin,pid_x2dx_satmax);
    PID_X2Dx.enableAntiWindup(pid_x2dx_enableantiwp,pid_x2dx_kw);

    *dx = PID_X2Dx.getOutput();


    /************************ Position Y Controller ( from Y to Dy ) *****************************/

    PID_Y2Dy.setGains(pid_y2dy_kp,pid_y2dy_ki,pid_y2dy_kd);
    PID_Y2Dy.enableMaxOutput(pid_y2dy_enablesat,pid_y2dy_satmin,pid_y2dy_satmax);
    PID_Y2Dy.enableAntiWindup(pid_y2dy_enableantiwp,pid_y2dy_kw);

    *dy = PID_Y2Dy.getOutput();
}
